import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Mascota } from './mascota';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MascotasService {
  private baseURL: string = 'http://localhost:8090/mascotas';

  constructor(private http: HttpClient) {
    console.log('se creo la instancia');
  }

  public getMascotas(): Observable<Mascota[]> {
    return this.http.get<Mascota[]>(`${this.baseURL}`);
  }

  
}
